package cz.hackprague.signfriendly

import cz.hackprague.signfriendly.data.datasource.local.SignRecordEntity
import cz.hackprague.signfriendly.ui.fragments.saved.MediaType
import cz.hackprague.signfriendly.ui.fragments.saved.SignRecord
import cz.hackprague.signfriendly.ui.main.LanguageType

fun SignRecordEntity.toSignRecord() = SignRecord(
    type = MediaType.valueOf(type),
    word = word,
    languageType = LanguageType.valueOf(languageType),
    videoUrl = videoUrl,
    imageUrls = imageUrls
)

fun SignRecord.toSignRecordEntity() = SignRecordEntity(
    id = 0,
    word = word,
    type = type.name,
    videoUrl = videoUrl,
    imageUrls = imageUrls,
    languageType = languageType.name
)