package cz.hackprague.signfriendly.ui

import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import cz.hackprague.signfriendly.databinding.MergeLoadingBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancelChildren

abstract class BaseFragment<Views : ViewBinding> : Fragment(), CoroutineScope by MainScope() {

    private var loadingOverlay: View? = null

    protected var views: Views? = null
        private set

    override fun onDestroy() {
        views = null
        coroutineContext.cancelChildren()
        super.onDestroy()
    }

    // View Binding

    protected fun setContentView(viewBinding: Views): View {
        views = viewBinding
        return viewBinding.root
    }

    protected fun setContentViewWithLoading(viewBinding: Views): View {
        views = viewBinding
        loadingOverlay = MergeLoadingBinding.bind(viewBinding.root).loading
        return viewBinding.root
    }

    protected fun views(input: Views.() -> Unit) {
        input.invoke(views ?: return)
    }

    protected fun loading(isLoading: Boolean) {
        loadingOverlay?.isVisible = isLoading
    }

    protected fun hideKeyboard() {
        activity?.currentFocus?.run {
            activity?.getSystemService(InputMethodManager::class.java)
                ?.hideSoftInputFromWindow(windowToken, 0)
        }
    }

}