package cz.hackprague.signfriendly.ui.main

import android.os.Bundle
import cz.hackprague.signfriendly.databinding.ActivitySplashBinding
import cz.hackprague.signfriendly.ui.BaseActivity
import cz.hackprague.signfriendly.ui.extensions.startNewActivity

class SplashActivity : BaseActivity<ActivitySplashBinding>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        startNewActivity(MainActivity::class.java)
    }
}