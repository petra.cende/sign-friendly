package cz.hackprague.signfriendly.ui.fragments.saved

import cz.hackprague.signfriendly.ui.main.LanguageType

data class SignRecord(
    val type: MediaType,
    val word: String,
    val languageType: LanguageType,
    val videoUrl: String? = null,
    val imageUrls: List<String>? = null
)