package cz.hackprague.signfriendly.ui.extensions

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment

fun Fragment.showDialog(dialogFragment: DialogFragment) {
    (activity as? AppCompatActivity)?.showDialog(dialogFragment)
}