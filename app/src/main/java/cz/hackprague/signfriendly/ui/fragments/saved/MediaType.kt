package cz.hackprague.signfriendly.ui.fragments.saved

enum class MediaType {
    VIDEO,
    IMAGE
}