package cz.hackprague.signfriendly.ui.dialogs

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import cz.hackprague.signfriendly.R
import cz.hackprague.signfriendly.databinding.FlagDialogBinding
import cz.hackprague.signfriendly.ui.main.LanguageType


class FlagDialog(
    private val onLanguageSelected: (LanguageType) -> Unit
) : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): AlertDialog {
        var languageType = LanguageType.ENGLISH
        val layout = FlagDialogBinding.inflate(layoutInflater)

        layout.btnClose.setOnClickListener {
            dialog?.dismiss()
        }
        layout.radioGroup.setOnCheckedChangeListener { _, checkedId ->
            languageType = when (checkedId) {
                R.id.german -> LanguageType.GERMAN
                R.id.czech -> LanguageType.CZECH
                else -> LanguageType.ENGLISH
            }
        }

        layout.confirm.setOnClickListener {
            onLanguageSelected.invoke(languageType)
            dialog?.dismiss()
        }

        return AlertDialog.Builder(requireContext())
            .setView(layout.root)
            .create().apply {
                window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            }

    }
}