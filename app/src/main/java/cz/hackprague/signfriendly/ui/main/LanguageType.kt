package cz.hackprague.signfriendly.ui.main

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import cz.hackprague.signfriendly.R

enum class LanguageType(
    val id: String,
    @StringRes val stringRes: Int,
    @DrawableRes val flagRes: Int
) {

    CZECH("Czech", R.string.czech, R.drawable.ic_czech),
    ENGLISH("English (United States)", R.string.english, R.drawable.ic_english),
    GERMAN("German (Germany)", R.string.german, R.drawable.ic_german);

    companion object {
        fun toLanguageType(position: Int): LanguageType = values()[position]
    }
}