package cz.hackprague.signfriendly.ui

import android.view.View
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.viewbinding.ViewBinding
import com.google.android.material.textview.MaterialTextView
import cz.hackprague.signfriendly.databinding.MergeLoadingBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope

abstract class BaseActivity<Views : ViewBinding> :
    AppCompatActivity(), CoroutineScope by MainScope() {

    private var loadingText: MaterialTextView? = null
    private lateinit var loadingOverlay: View

    protected lateinit var views: Views
        private set

    protected fun views(block: Views.() -> Unit) {
        block.invoke(views)
    }

    protected fun setContentView(viewBinding: Views) {
        super.setContentView(viewBinding.root)
        views = viewBinding
    }

    protected fun setContentViewWithLoading(viewBinding: Views) {
        setContentView(viewBinding)
        loadingOverlay = MergeLoadingBinding.bind(viewBinding.root).loading
        loadingText = MergeLoadingBinding.bind(viewBinding.root).loadingText
    }

    protected fun loading(
        isLoading: Boolean,
        @StringRes loadingTextRes: Int? = null
    ) {
        loadingOverlay.isVisible = isLoading

        if (isLoading && loadingTextRes != null) {
            loadingText?.setText(loadingTextRes)
        }

        loadingText?.isVisible = isLoading
    }


}