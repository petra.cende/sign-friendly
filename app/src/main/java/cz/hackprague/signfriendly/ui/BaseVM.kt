package cz.hackprague.signfriendly.ui

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

abstract class BaseVM : ViewModel(), CoroutineScope {

    private val job: Job = SupervisorJob()

    override val coroutineContext: CoroutineContext = Dispatchers.Default + job

    override fun onCleared() {
        coroutineContext.cancelChildren()
        super.onCleared()
    }

}