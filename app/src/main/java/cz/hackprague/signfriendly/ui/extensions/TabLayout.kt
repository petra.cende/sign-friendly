package cz.hackprague.signfriendly.ui.extensions

import androidx.annotation.DrawableRes
import com.google.android.material.tabs.TabLayout


fun TabLayout.setupTab(
    index: Int,
    @DrawableRes iconRes: Int
) {
    getTabAt(index)?.setIcon(iconRes)
}