package cz.hackprague.signfriendly.ui

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentPagerAdapter
import cz.hackprague.signfriendly.R
import cz.hackprague.signfriendly.ui.fragments.HistoryFragment
import cz.hackprague.signfriendly.ui.fragments.translator.TranslatorFragment
import cz.hackprague.signfriendly.ui.fragments.saved.SavedSignsFragment

class MainTabsAdapter(
    private val activity: FragmentActivity
) : FragmentPagerAdapter(
    activity.supportFragmentManager,
    BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
) {

    companion object {
        const val TRANSLATOR_TAB_INDEX = 0
        const val HISTORY_TAB_INDEX = 1
        const val SAVED_TAB_INDEX = 2
    }

    private val input by lazy { TranslatorFragment() }
    private val history by lazy { HistoryFragment() }
    private val saved by lazy { SavedSignsFragment() }

    override fun getCount(): Int = 3

    override fun getItem(position: Int): Fragment = when (position) {
        TRANSLATOR_TAB_INDEX -> input
        HISTORY_TAB_INDEX -> history
        SAVED_TAB_INDEX -> saved
        else -> throw Exception("Unknown tab index.")
    }

    override fun getPageTitle(position: Int): String = activity.getString(
        when (position) {
            TRANSLATOR_TAB_INDEX -> R.string.interpreter
            HISTORY_TAB_INDEX -> R.string.history
            SAVED_TAB_INDEX -> R.string.saved_records
            else -> throw Exception("Unknown tab index.")
        }
    )
}