package cz.hackprague.signfriendly.ui.extensions

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData

fun <T : Any> LifecycleOwner.observe(
    data: LiveData<T>,
    observer: (T) -> Unit
) {
    data.observe(this, { observer(it) })
}
