package cz.hackprague.signfriendly.ui.fragments.saved

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import cz.hackprague.signfriendly.databinding.ItemImageRecordBinding
import cz.hackprague.signfriendly.databinding.ItemVideoRecordBinding

class SavedSignsAdapter(
    context: Context
) : ListAdapter<SignRecord, RecyclerView.ViewHolder>(DIFF_UTIL) {

    private companion object {
        private val DIFF_UTIL = object : DiffUtil.ItemCallback<SignRecord>() {
            override fun areItemsTheSame(
                oldItem: SignRecord,
                newItem: SignRecord
            ) = (oldItem.word == newItem.word)

            override fun areContentsTheSame(
                oldItem: SignRecord,
                newItem: SignRecord
            ) = oldItem == newItem
        }

        private const val VIDEO = 0
        private const val IMAGE = 1
    }

    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            VIDEO -> VideoHolder(ItemVideoRecordBinding.inflate(inflater))
            IMAGE -> ImageHolder(ItemImageRecordBinding.inflate(inflater))
            else -> VideoHolder(ItemVideoRecordBinding.inflate(inflater))

        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        with(currentList[position]) {
            when (holder) {
                is VideoHolder -> holder.bind(this)
                is ImageHolder -> holder.bind(this)
            }
        }
    }

    override fun getItemCount(): Int = currentList.size

    inner class VideoHolder(private val viewBinding: ItemVideoRecordBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {

        fun bind(sign: SignRecord) {
            with(viewBinding) {
                inputWord.text = sign.word
                flag.load(sign.languageType.flagRes)
            }
        }

    }

    inner class ImageHolder(private val viewBinding: ItemImageRecordBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {

        fun bind(sign: SignRecord) {
            with(viewBinding) {
                inputWord.text = sign.word
                flag.load(sign.languageType.flagRes)
            }
        }
    }
}