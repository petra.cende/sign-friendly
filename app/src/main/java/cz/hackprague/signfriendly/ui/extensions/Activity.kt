package cz.hackprague.signfriendly.ui.extensions

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment

fun <T : AppCompatActivity> AppCompatActivity.startNewActivity(
    target: Class<T>,
    bundle: Bundle? = null
) {
    val component = Intent(this, target).component

    with(Intent.makeRestartActivityTask(component)) {
        bundle?.let { putExtras(bundle) }
        startActivity(this)
        finish()
    }
}


fun AppCompatActivity.showDialog(dialogFragment: DialogFragment) {
    val dialogFragmentSimpleName = dialogFragment::class.simpleName

    supportFragmentManager.fragments.forEach { fragment ->
        // prevent from multiple opening
        if (dialogFragmentSimpleName == fragment::class.simpleName) {
            return
        }
    }

    dialogFragment.show(supportFragmentManager, dialogFragmentSimpleName)
}