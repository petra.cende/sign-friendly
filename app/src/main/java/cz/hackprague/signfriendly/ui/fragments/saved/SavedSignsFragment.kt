package cz.hackprague.signfriendly.ui.fragments.saved

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cz.hackprague.signfriendly.databinding.FragmentSavedSignsBinding
import cz.hackprague.signfriendly.ui.BaseFragment
import cz.hackprague.signfriendly.ui.dialogs.ErrorDialog
import cz.hackprague.signfriendly.ui.extensions.observe
import cz.hackprague.signfriendly.ui.extensions.showDialog
import cz.hackprague.signfriendly.ui.fragments.saved.SavedSignsVM.SignsState
import org.koin.androidx.viewmodel.ext.android.viewModel

class SavedSignsFragment : BaseFragment<FragmentSavedSignsBinding>() {

    private val savedSignsVM: SavedSignsVM by viewModel()

    private val savedSignsAdapter: SavedSignsAdapter by lazy { SavedSignsAdapter(requireContext()) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = setContentViewWithLoading(FragmentSavedSignsBinding.inflate(layoutInflater))

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        views {
            records.adapter = savedSignsAdapter
            observe(savedSignsVM.signsState) {
                observeSignsState(it)
            }
        }
    }

    private fun observeSignsState(state: SignsState) {
        loading(state == SignsState.Loading)

        when (state) {
            SignsState.Empty -> {

            }
            SignsState.Error -> showDialog(ErrorDialog())
            is SignsState.Loaded -> savedSignsAdapter.submitList(state.records)
            else -> {

            }
        }


    }

}