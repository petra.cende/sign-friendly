package cz.hackprague.signfriendly.ui.dialogs

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import cz.hackprague.signfriendly.databinding.ErrorDialogBinding

class ErrorDialog : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): AlertDialog {
        val layout = ErrorDialogBinding.inflate(layoutInflater)

        layout.btnClose.setOnClickListener {
            dialog?.dismiss()
        }

        return AlertDialog.Builder(requireContext())
            .setView(layout.root)
            .create().apply {
                window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            }

    }

}