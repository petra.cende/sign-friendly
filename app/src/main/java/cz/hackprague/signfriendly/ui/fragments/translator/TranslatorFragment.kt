package cz.hackprague.signfriendly.ui.fragments.translator

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.view.isVisible
import coil.load
import cz.hackprague.signfriendly.databinding.FragmentTranslatorBinding
import cz.hackprague.signfriendly.databinding.MergeSearchBinding
import cz.hackprague.signfriendly.domain.TranslationsResult
import cz.hackprague.signfriendly.ui.BaseFragment
import cz.hackprague.signfriendly.ui.dialogs.ErrorDialog
import cz.hackprague.signfriendly.ui.dialogs.FlagDialog
import cz.hackprague.signfriendly.ui.extensions.observe
import cz.hackprague.signfriendly.ui.extensions.showDialog
import cz.hackprague.signfriendly.ui.extensions.toText
import cz.hackprague.signfriendly.ui.main.LanguageType
import org.koin.androidx.viewmodel.ext.android.viewModel

class TranslatorFragment : BaseFragment<FragmentTranslatorBinding>() {

    private val translatorVM: TranslatorVM by viewModel()

    private var searchViews: MergeSearchBinding? = null
    private var languageType = LanguageType.ENGLISH

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = setContentViewWithLoading(FragmentTranslatorBinding.inflate(layoutInflater)).also {
        searchViews = MergeSearchBinding.bind(it)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
        observe(translatorVM.translatorState) {
            observeInputState(it)
        }
    }


    private fun observeInputState(inputState: TranslatorVM.TranslatorState) {
        loading(inputState == TranslatorVM.TranslatorState.Loading)

        when (inputState) {
            TranslatorVM.TranslatorState.Error -> showDialog(ErrorDialog())
            TranslatorVM.TranslatorState.EmptyWord -> {
                views {

                }
            }
            is TranslatorVM.TranslatorState.Loaded -> {
                prepareTranslationResult(inputState.translationsResult)
            }
            else -> {

            }
        }
    }

    private fun prepareTranslationResult(result: TranslationsResult) {
        views {
            groupResult.isVisible = true
            placeholder.isVisible = false
        }
        setupImages(result.imageUrls)
        setupVideo(result.videoUrl)
    }

    private fun setupViews() {
        searchViews?.run {
            searchButton.setOnClickListener {
                translatorVM.sendInput(
                    languageType,
                    searchInput.toText()
                )
                hideKeyboard()
            }

            flag.setOnClickListener {
                showDialog(FlagDialog() {
                    languageType = it
                    flag.setImageResource(it.flagRes)
                })
            }
        }
    }

    private fun setupVideo(url: String) {
        views {
            with(videoView) {
                videoLayout.isVisible = true
                setVideoPath(url)
                setMediaController(mediaController)
                start()
            }
        }
    }

    private fun setupImages(imageUrls: List<String>) {
        views {
            imageUrls.forEach { url ->
                val imageView = AppCompatImageView(requireContext()).apply {
                    load(url)
                }
                images.addView(imageView)
            }
        }
    }

}