package cz.hackprague.signfriendly.ui.fragments.saved

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cz.hackprague.signfriendly.data.common.onSuccess
import cz.hackprague.signfriendly.domain.GetSavedSignsUseCase
import cz.hackprague.signfriendly.ui.BaseVM
import kotlinx.coroutines.launch

class SavedSignsVM(
    private val getSavedSigns: GetSavedSignsUseCase
) : BaseVM() {

    private val _signsState: MutableLiveData<SignsState> = MutableLiveData()
    val signsState: LiveData<SignsState> = _signsState

    fun getSavedSigns() {
        launch {
            _signsState.postValue(SignsState.Loading)

            getSavedSigns.execute().onSuccess {
                _signsState.postValue(SignsState.Loaded(it))
            }
        }
    }


    sealed class SignsState {
        object Loading : SignsState()
        object Empty : SignsState()
        data class Loaded(val records: List<SignRecord>) : SignsState()
        object Error : SignsState()
    }

}