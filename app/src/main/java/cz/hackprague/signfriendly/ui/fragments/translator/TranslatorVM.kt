package cz.hackprague.signfriendly.ui.fragments.translator

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cz.hackprague.signfriendly.data.common.onFailure
import cz.hackprague.signfriendly.data.common.onSuccess
import cz.hackprague.signfriendly.domain.SendInputUseCase
import cz.hackprague.signfriendly.domain.TranslationsResult
import cz.hackprague.signfriendly.ui.BaseVM
import cz.hackprague.signfriendly.ui.main.LanguageType
import kotlinx.coroutines.launch

class TranslatorVM(
    private val sendInput: SendInputUseCase,
) : BaseVM() {

    private val _translatorState: MutableLiveData<TranslatorState> = MutableLiveData()
    val translatorState: LiveData<TranslatorState> = _translatorState

    fun sendInput(language: LanguageType, word: String) {
        launch {
            _translatorState.postValue(TranslatorState.Loading)

            if (word.isEmpty()) {
                _translatorState.postValue(TranslatorState.EmptyWord)
                return@launch
            }

            sendInput.execute(
                SendInputUseCase.WordInput(language.id, word)
            ).onSuccess {
                _translatorState.postValue(TranslatorState.Loaded(it))
            }.onFailure {
                _translatorState.postValue(TranslatorState.Error)
            }
        }
    }

    sealed class TranslatorState {
        object Loading : TranslatorState()
        object EmptyWord : TranslatorState()
        data class Loaded(val translationsResult: TranslationsResult) : TranslatorState()
        object Error : TranslatorState()
    }
}