package cz.hackprague.signfriendly.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cz.hackprague.signfriendly.databinding.FragmentHistoryBinding
import cz.hackprague.signfriendly.ui.BaseFragment

class HistoryFragment : BaseFragment<FragmentHistoryBinding>() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = setContentViewWithLoading(
        cz.hackprague.signfriendly.databinding.FragmentHistoryBinding.inflate(layoutInflater)
    )
}