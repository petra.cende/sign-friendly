package cz.hackprague.signfriendly.ui.extensions

import android.text.Editable
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import com.google.android.material.textfield.TextInputEditText

fun EditText.toText(): String = text.toString()