package cz.hackprague.signfriendly.ui.extensions

import androidx.annotation.StringRes
import com.google.android.material.textfield.TextInputLayout

fun TextInputLayout.onValidityChanged(
    isValid: Boolean,
    errorMessage: String? = null
) {
    error = if (isValid) {
        clearError()
        null
    } else {
        errorMessage
    }
}

fun TextInputLayout.markRequired() {
    hint = "$hint *"
}

fun TextInputLayout.setupError(@StringRes errorRes: Int) {
    error = context.getString(errorRes)
    isErrorEnabled = true
}


fun TextInputLayout.clearError() {
    error = null
    isErrorEnabled = false
}