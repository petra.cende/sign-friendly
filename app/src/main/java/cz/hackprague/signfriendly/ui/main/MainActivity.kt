package cz.hackprague.signfriendly.ui.main

import android.os.Bundle
import cz.hackprague.signfriendly.R
import cz.hackprague.signfriendly.databinding.ActivityMainBinding
import cz.hackprague.signfriendly.ui.BaseActivity
import cz.hackprague.signfriendly.ui.MainTabsAdapter
import cz.hackprague.signfriendly.ui.extensions.setupTab
import cz.hackprague.signfriendly.ui.fragments.translator.TranslatorVM
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity<ActivityMainBinding>() {

    private val translatorVM: TranslatorVM by viewModel()

    private val mainTabsAdapter = MainTabsAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentViewWithLoading(ActivityMainBinding.inflate(layoutInflater))

        views {
            viewPager.adapter = mainTabsAdapter
            with(tabLayout) {
                setupWithViewPager(viewPager)
                setupTab(
                    MainTabsAdapter.TRANSLATOR_TAB_INDEX,
                    R.drawable.ic_translator
                )
                setupTab(MainTabsAdapter.HISTORY_TAB_INDEX, R.drawable.ic_history)
                setupTab(MainTabsAdapter.SAVED_TAB_INDEX, R.drawable.ic_star)

            }
        }
    }


}