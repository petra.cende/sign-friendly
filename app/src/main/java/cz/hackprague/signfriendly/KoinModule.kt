package cz.hackprague.signfriendly

import android.app.Application
import androidx.room.Room
import cz.hackprague.signfriendly.data.datasource.local.LocalDataSourceApi
import cz.hackprague.signfriendly.data.datasource.local.LocalDataSourceImpl
import cz.hackprague.signfriendly.data.datasource.local.SignRecordDatabase
import cz.hackprague.signfriendly.data.datasource.remote.NetworkDataSourceApi
import cz.hackprague.signfriendly.data.datasource.remote.NetworkDataSourceImpl
import cz.hackprague.signfriendly.data.repository.TranslationsRepositoryImpl
import cz.hackprague.signfriendly.domain.GetSavedSignsUseCase
import cz.hackprague.signfriendly.domain.SendInputUseCase
import cz.hackprague.signfriendly.domain.TranslationsRepositoryApi
import cz.hackprague.signfriendly.ui.fragments.saved.SavedSignsVM
import cz.hackprague.signfriendly.ui.fragments.translator.TranslatorVM
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

fun viewModelsModule(): Module = module {
    viewModel { TranslatorVM(get()) }
    viewModel { SavedSignsVM(get()) }
}

fun domainModule(): Module = module {
    single<TranslationsRepositoryApi> {
        TranslationsRepositoryImpl(get())
    }

    single { SendInputUseCase(get()) }
    single { GetSavedSignsUseCase(get()) }
}

fun dataModule(): Module = module {
    fun provideDatabase(application: Application): SignRecordDatabase {
        return Room.databaseBuilder(
            application,
            SignRecordDatabase::class.java, "signs.db"
        )
            .fallbackToDestructiveMigration()
            .build()
    }

    single { provideDatabase(androidApplication()) }
    // data sources
    single<NetworkDataSourceApi> { NetworkDataSourceImpl(androidApplication()) }
    single<LocalDataSourceApi> { LocalDataSourceImpl(get()) }
}