package cz.hackprague.signfriendly.data.common

sealed class Result<out T : Any, out E : Exception>

data class Success<out T : Any>(val value: T) : Result<T, Nothing>()

data class Failure<out E : Exception>(val exception: E) : Result<Nothing, E>()

inline fun <T : Any, E : Exception> Result<T, E>.onSuccess(
    action: (T) -> Unit
): Result<T, E> = also {
    if (this is Success) {
        action(value)
    }
}

inline fun <T : Any, E : Exception> Result<T, E>.onFailure(
    action: (E) -> Unit
): Result<T, E> = also {
    if (this is Failure) {
        action(exception)
    }
}

inline fun <T : Any, E : Exception> Result<T, E>.onFailed(
    action: Failure<E>.() -> Nothing
): T = when (this) {
    is Success -> value
    is Failure -> action(this)
}