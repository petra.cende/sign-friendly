package cz.hackprague.signfriendly.data.datasource.remote

import com.google.gson.annotations.SerializedName

data class TranslationResponse(
    @SerializedName("video_url") val videoUrl: String,
    @SerializedName("image_urls") val imageUrls: List<String>
)