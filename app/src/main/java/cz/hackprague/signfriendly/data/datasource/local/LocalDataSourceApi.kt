package cz.hackprague.signfriendly.data.datasource.local

import cz.hackprague.signfriendly.data.common.Result
import cz.hackprague.signfriendly.ui.fragments.saved.SignRecord
import kotlinx.coroutines.flow.Flow

interface LocalDataSourceApi {

    suspend fun getRecords(): Flow<Result<List<SignRecord>, Exception>>

    suspend fun createRecord(signRecord: SignRecord)
}