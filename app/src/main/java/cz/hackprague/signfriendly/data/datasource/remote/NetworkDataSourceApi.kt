package cz.hackprague.signfriendly.data.datasource.remote

import cz.hackprague.signfriendly.data.common.Result
import cz.hackprague.signfriendly.domain.TranslationsResult
import cz.hackprague.signfriendly.domain.WordModel

interface NetworkDataSourceApi {

    suspend fun getTranslations(word: WordModel): Result<TranslationsResult, Exception>
}