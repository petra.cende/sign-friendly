package cz.hackprague.signfriendly.data.datasource.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface SignsDatabaseDao {

    @Query("SELECT * FROM sign_record")
    suspend fun getAll(): Flow<List<SignRecordEntity>>

    @Insert
    suspend fun createRecord(signRecordEntity: SignRecordEntity)
}