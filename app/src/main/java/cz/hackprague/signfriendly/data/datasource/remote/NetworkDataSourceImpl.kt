package cz.hackprague.signfriendly.data.datasource.remote

import android.app.Application
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.google.gson.Gson
import cz.hackprague.signfriendly.data.common.Failure
import cz.hackprague.signfriendly.data.common.Result
import cz.hackprague.signfriendly.data.common.Success
import cz.hackprague.signfriendly.domain.TranslationsResult
import cz.hackprague.signfriendly.domain.WordModel
import cz.hackprague.signfriendly.domain.toTranslationResult
import cz.hackprague.signfriendly.domain.toWordRequest
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit

class NetworkDataSourceImpl(
    private val app: Application,
): NetworkDataSourceApi {

    private companion object {
        private const val BASE_URL = "https://api.apify.com/"
        private const val APIFY_TOKEN = "apify_api_A3j4e2hWnvg6EESe8bCvqP6LmhpGb64oq80g"
        private const val ACTOR_ID = "MKwAtgWMnD4qOE3El"
    }

    private val okHttpClient = OkHttpClient.Builder()
        .connectTimeout(10, TimeUnit.MINUTES)
        .writeTimeout(10, TimeUnit.MINUTES)
        .readTimeout(30, TimeUnit.MINUTES)
        .addNetworkInterceptor(ChuckerInterceptor.Builder(app).build())
        .build();

    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create(Gson()))
        .build()

    private val apifyServices = retrofit.create(ApifyServices::class.java)

    override suspend fun getTranslations(word: WordModel): Result<TranslationsResult, Exception> =
        try {
            val result = apifyServices.getTranslations(ACTOR_ID, APIFY_TOKEN, word.toWordRequest())
                .toTranslationResult()
            Success(result)
        } catch (e: Exception) {
            Timber.e("Error due to: $e")
            Failure(e)
        }
}