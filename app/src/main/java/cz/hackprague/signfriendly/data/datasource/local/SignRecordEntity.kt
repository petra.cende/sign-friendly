package cz.hackprague.signfriendly.data.datasource.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "sign_record")
data class SignRecordEntity(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "word") val word: String,
    @ColumnInfo(name = "type") val type: String,
    @ColumnInfo(name = "language_type") val languageType: String,
    @ColumnInfo(name = "video_url") val videoUrl: String?,
    @ColumnInfo(name = "image_urls") val imageUrls: List<String>?,
)