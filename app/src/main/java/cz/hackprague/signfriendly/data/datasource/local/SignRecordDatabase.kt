package cz.hackprague.signfriendly.data.datasource.local

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [SignRecordEntity::class], version = 1)
abstract class SignRecordDatabase : RoomDatabase() {

    abstract fun signsDao(): SignsDatabaseDao
}