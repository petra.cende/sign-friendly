package cz.hackprague.signfriendly.data.datasource.remote

import cz.hackprague.signfriendly.data.datasource.WordRequest
import retrofit2.http.*

interface ApifyServices {

    @Headers("Accept: application/json")
    @POST("/v2/acts/{id}/run-sync")
    suspend fun getTranslations(
        @Path("id") actorId: String,
        @Query("token") token: String,
        @Body wordRequest: WordRequest
    ): TranslationResponse
}