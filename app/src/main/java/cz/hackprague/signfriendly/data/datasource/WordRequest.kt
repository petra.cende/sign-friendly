package cz.hackprague.signfriendly.data.datasource

data class WordRequest(
    val language: String,
    val word: String
)