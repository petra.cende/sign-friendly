package cz.hackprague.signfriendly.data.repository

import cz.hackprague.signfriendly.data.common.Result
import cz.hackprague.signfriendly.data.common.Success
import cz.hackprague.signfriendly.data.datasource.remote.NetworkDataSourceApi
import cz.hackprague.signfriendly.domain.TranslationsRepositoryApi
import cz.hackprague.signfriendly.domain.TranslationsResult
import cz.hackprague.signfriendly.domain.WordModel
import cz.hackprague.signfriendly.ui.fragments.saved.MediaType
import cz.hackprague.signfriendly.ui.fragments.saved.SignRecord
import cz.hackprague.signfriendly.ui.main.LanguageType

class TranslationsRepositoryImpl(
    private val networkDataSource: NetworkDataSourceApi,
//    private val localDataSource: LocalDataSourceApi
) : TranslationsRepositoryApi {

    override suspend fun getTranslations(wordModel: WordModel): Result<TranslationsResult, Exception> {
        return networkDataSource.getTranslations(wordModel)
    }

    override suspend fun getSavedSigns(): Result<List<SignRecord>, Exception> {
        return Success(
            listOf<SignRecord>(
                SignRecord(
                    type = MediaType.VIDEO,
                    word = "ahoj",
                    languageType = LanguageType.CZECH
                )
            )
        )
    }

}