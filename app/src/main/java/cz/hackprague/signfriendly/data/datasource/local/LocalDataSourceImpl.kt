package cz.hackprague.signfriendly.data.datasource.local

import cz.hackprague.signfriendly.data.common.Result
import cz.hackprague.signfriendly.data.common.Success
import cz.hackprague.signfriendly.toSignRecord
import cz.hackprague.signfriendly.toSignRecordEntity
import cz.hackprague.signfriendly.ui.fragments.saved.SignRecord
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class LocalDataSourceImpl(
   private val db: SignRecordDatabase
) : LocalDataSourceApi {

    override suspend fun getRecords(): Flow<Result<List<SignRecord>, Exception>> {
        return db.signsDao().getAll().map {
            Success(it.map { record -> record.toSignRecord() })
        }
    }

    override suspend fun createRecord(signRecord: SignRecord) {
        db.signsDao().createRecord(signRecord.toSignRecordEntity())
    }
}