package cz.hackprague.signfriendly.domain

import cz.hackprague.signfriendly.data.common.Result
import cz.hackprague.signfriendly.ui.fragments.saved.SignRecord

class GetSavedSignsUseCase(
    private val translationsRepositoryApi: TranslationsRepositoryApi
) : BaseOutputUseCase<Result<List<SignRecord>, Exception>>() {

    override suspend fun execute(): Result<List<SignRecord>, Exception> =
        translationsRepositoryApi.getSavedSigns()

}