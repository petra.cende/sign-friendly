package cz.hackprague.signfriendly.domain

import cz.hackprague.signfriendly.data.datasource.WordRequest
import cz.hackprague.signfriendly.data.datasource.remote.TranslationResponse

fun WordModel.toWordRequest() = WordRequest(
    word = word,
    language = languageType
)

fun TranslationResponse.toTranslationResult() = TranslationsResult(
    videoUrl = videoUrl,
    imageUrls = imageUrls
)