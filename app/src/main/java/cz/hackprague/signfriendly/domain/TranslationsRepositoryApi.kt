package cz.hackprague.signfriendly.domain

import cz.hackprague.signfriendly.data.common.Result
import cz.hackprague.signfriendly.ui.fragments.saved.SignRecord

interface TranslationsRepositoryApi {

    suspend fun getTranslations(wordModel: WordModel): Result<TranslationsResult, Exception>

    suspend fun getSavedSigns(): Result<List<SignRecord>, Exception>
}