package cz.hackprague.signfriendly.domain

abstract class BaseOutputUseCase<out Output> {

    abstract suspend fun execute(): Output
}

abstract class BaseInputOutputUseCase<in Input, out Output> {

    abstract suspend fun execute(input: Input): Output
}
