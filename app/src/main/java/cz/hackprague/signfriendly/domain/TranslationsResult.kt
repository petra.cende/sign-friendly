package cz.hackprague.signfriendly.domain

data class TranslationsResult(
    val videoUrl: String,
    val imageUrls: List<String>
)