package cz.hackprague.signfriendly.domain

data class WordModel(
    val languageType: String,
    val word: String
)