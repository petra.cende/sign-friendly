package cz.hackprague.signfriendly.domain

import cz.hackprague.signfriendly.data.common.Result

class SendInputUseCase(
    private val translationsRepository: TranslationsRepositoryApi
) :
    BaseInputOutputUseCase<SendInputUseCase.WordInput, Result<TranslationsResult, Exception>>() {

    override suspend fun execute(input: WordInput): Result<TranslationsResult, Exception> {
        return translationsRepository.getTranslations(
            WordModel(
                languageType = input.languageName,
                word = input.word
            )
        )
    }

    data class WordInput(
        val languageName: String,
        val word: String
    )
}